.PHONY: all bootstrap deploy ports clean tests

all: bootstrap deploy ports

bootstrap:
	@echo "Running bootstrap script..."
	@./scripts/bootstrap.sh

deploy:
	@echo "Running Ansible playbook..."
	@ansible-playbook prometheus_monitoring_stack.yaml

ports:
	@echo "Starting port forwarding..."
	@./scripts/start_port_forwarding.sh

clean:
	@echo "Removing the cluster..."
	@./scripts/remove_cluster.sh

tests:
	cd tests ; \
	./run_all_tests.sh
