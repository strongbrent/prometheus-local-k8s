#!/usr/bin/env bats
# File: tests/create_k3d_cluster.bats

@test "Check if k3d cluster 'prometheus-local' is created successfully" {
  run k3d cluster list
  [ "$status" -eq 0 ]
  [[ $output =~ "prometheus-local" ]]
  [[ $output =~ "1/1" ]]
  [[ $output =~ "0/0" ]]
  [[ $output =~ "true" ]]
}

@test "Check to ensure that the k3d cluster configuration file has been removed" {
  run test ! -f /tmp/k3d-prometheus-local-config.yaml
  [ "$status" -eq 0 ]
}