#!/bin/bash

# Get a list of all screen sessions that match any of the specified names
screen_sessions=$(screen -ls | grep -E 'prometheus_port_forward|alertmanager_port_forward|grafana_port_forward' | awk -F '.' '{print $1}' | awk '{print $1}')

# Loop through the list of screen sessions and kill each one
for session_id in $screen_sessions; do
    screen -S "$session_id" -X quit
done

# Then delete the cluster
k3d cluster delete prometheus-local 