# Prometheus Running on a Local K3D Cluster
This is an Ansible role that I created to run the Prometheus Operator on my workstation in a local or development kubernetes cluster running via [k3d](https://k3d.io). If you are not familiar with [k3d](https://k3d.io), it's a lightweight wrapper to run [k3s](https://k3s.io/) ([Rancher Lab’s](https://www.rancher.com/) minimal Kubernetes distribution) in docker. Basically [k3d](https://k3d.io) makes it very easy to create single- and multi-node k3s clusters in docker, e.g. for local development on Kubernetes.

## NOTES

It is beyond the scope of this project to teach you how to install k3d. For installation instructions, please read the [Installation Section](https://k3d.io/v5.6.0/#installation) of k3d's main web site.

For this installation of [Prometheus](https://prometheus.io/), [Alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/), and [Grafana](https://grafana.com/), I am using the following kubernetes operator:
- [Prometheus Community Kubernetes Helm Charts](https://github.com/prometheus-community/helm-charts/)

## Requirements

I have developed this role to run exclusively on my trusty old [Lenovo T480](https://www.pcmag.com/reviews/lenovo-thinkpad-t480s) workstation which is running [Fedora 39](https://fedoraproject.org/workstation/download). You can modify this role to run on any Linux distro; however, you would have to slightly modify the bootstrap script (see section on Ansible dependencies below). This could also be adapted to run on a Mac, as you could use [Homebrew](https://formulae.brew.sh/formula/k3d) to install k3d, as well as the other various dependencies.

To provision and configure this operator, I needed to install a small number of required applications:

- [git](https://git-scm.com/)
- `make` command
- [helm](https://helm.sh/) 
- [kublectl](https://kubernetes.io/docs/reference/kubectl/)
- [k3d](https://k3d.io/)
- [Bats](https://bats-core.readthedocs.io/en/stable/index.html) (for running the verification tests)

Again, for these programs, you are on your own... Even so, I have included a simple [bootstrap.sh](scripts/bootstrap.sh) script that I used to install these Ansible dependencies:

- Fedora package: [python3-kubernetes](https://github.com/kubernetes-client/python)
- Ansible Galaxy Collection: [kubernetes.core](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/index.html)
- Ansible Galaxy Collection: [community.kubernetes](https://docs.ansible.com/ansible/2.10/collections/community/kubernetes/k8s_module.html) - I think that this one might be deprecated? So I might remove it at some point...

## Quickstart

### Install Everything All At Once

To kick everything off, I cloned this repo and then navigated to the repo directory.

Then I ran the following command to install the Ansible dependencies, create the k3d cluster, deploy the operator, and forward the ports for the Prometheus, Alertmanager, and Grafana UIs:
```
make all
[example output]
Running bootstrap script...
Last metadata expiration check: 2:14:47 ago on Sat 23 Mar 2024 10:34:11 AM PDT.
Dependencies resolved.
Nothing to do.
Complete!
Last metadata expiration check: 2:14:49 ago on Sat 23 Mar 2024 10:34:11 AM PDT.
Package python3-kubernetes-1:29.0.0-1.fc39.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
Starting galaxy collection install process
Nothing to do. All requested collections are already installed. If you want to reinstall them, consider using `--force`.
Starting galaxy collection install process
Nothing to do. All requested collections are already installed. If you want to reinstall them, consider using `--force`.
Running Ansible playbook...

PLAY [localhost] ***************************************************************

TASK [prometheus-local : Check if k3d cluster 'prometheus-local' already exists] ***
ok: [localhost]

TASK [prometheus-local : Include create_k3d_cluster.yaml] **********************
included: /home/brentwg/Projects/Ansible/prometheus-k8s/roles/prometheus-local/tasks/create_k3d_cluster.yaml for localhost

TASK [prometheus-local : Generate k3d cluster configuration file] **************
changed: [localhost]

TASK [prometheus-local : Create k3d cluster using the generated configuration file] ***
changed: [localhost]

TASK [prometheus-local : Remove k3d cluster configuration file] ****************
changed: [localhost]

TASK [prometheus-local : Pause for 15 seconds] *********************************
Pausing for 15 seconds
(ctrl+C then 'C' = continue early, ctrl+C then 'A' = abort)
ok: [localhost]

TASK [prometheus-local : Check if Prometheus Operator is already installed] ****
ok: [localhost]

TASK [prometheus-local : Include deploy_prometheus_operator.yaml] **************
included: /home/brentwg/Projects/Ansible/prometheus-k8s/roles/prometheus-local/tasks/deploy_prometheus_operator.yaml for localhost

TASK [prometheus-local : Create namespace for Prometheus Operator] *************
changed: [localhost]

TASK [prometheus-local : Add the Prometheus community Helm repository] *********
ok: [localhost]

TASK [prometheus-local : Generate custom values file for Prometheus Operator] ***
changed: [localhost]

TASK [prometheus-local : Install Prometheus Operator] **************************
changed: [localhost]

TASK [prometheus-local : Remove the Prometheus custom values file] *************
changed: [localhost]

PLAY RECAP *********************************************************************
localhost                  : ok=13   changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

Starting port forwarding...
Waiting for service prometheus-operator-kube-p-prometheus to become available...
service prometheus-operator-kube-p-prometheus is now available.
Starting screen session to forward port for Prometheus...
Waiting for service prometheus-operator-kube-p-alertmanager to become available...
service prometheus-operator-kube-p-alertmanager is now available.
Starting screen session to forward port for Alertmanager...
Waiting for service prometheus-operator-grafana to become available...
service prometheus-operator-grafana is now available.
Starting screen session to forward port for Grafana...
Port-forwarding started in separate screen sessions.
```

Simple as that. Once everything has completed successfully, you should be able to access the following dashboards:

- Prometheus: [http://localhost:9090/](http://localhost:9090/)
- Alertmanager: [http://localhost:9093/](http://localhost:9093/)
- Grafana: [http://localhost:8080/](http://localhost:8080/) -> default username/password is: `admin`/`prom-operator`

### Destroying the Cluster
If you want to remove the whole works at once, execute the following:
```
make clean
[example output]
Removing the cluster...
INFO[0000] Deleting cluster 'prometheus-local'          
INFO[0002] Deleting cluster network 'k3d-prometheus-local' 
INFO[0002] Deleting 1 attached volumes...               
INFO[0002] Removing cluster details from default kubeconfig... 
INFO[0002] Removing standalone kubeconfig file (if there is one)... 
INFO[0002] Successfully deleted cluster prometheus-local!  
```

### Validating the Deployment
If you want to verify that everything is set up correctly, execute the following:
```
make tests
[sample output]
cd tests ; \
./run_all_tests.sh
Executing test file: create_k3d_cluster.bats
create_k3d_cluster.bats
 ✓ Check if k3d cluster 'prometheus-local' is created successfully
 ✓ Check to ensure that the k3d cluster configuration file has been removed

2 tests, 0 failures

Executing test file: deploy_prometheus_operator.bats
deploy_prometheus_operator.bats
 ✓ Check if Kubernetes namespace 'monitoring' has been created
 ✓ Check to see if the correct Helm repo has been installed
 ✓ Check to ensure that the Prometheus operator custom values file has been removed
 ✓ Ensure that the Prometheus service is running
 ✓ Ensure that the Alertmanager service is running
 ✓ Ensure that the Grafana service is running
 ✓ Verify Persistent Volumes
 ✓ Verify Persistent Volume Claims
 ✓ Ensure that the Prometheus port has been forwarded successfully
 ✓ Ensure that the Alertmanager port has been forwarded successfully
 ✓ Ensure that the Grafana port has been forwarded successfully

11 tests, 0 failures
```

### Customizations
I've included a template called [custom_values.yaml.j2](roles/prometheus-local/templates/custom_values.yaml.j2) which can be modified to customize the Prometheus operator as much as you like. 

## TODO
- remove `pause` commands from Ansible tasks and replace them with some methodolody to poll k8s to ensure that the cluster is ready before attempting to run the operator installation tasks
