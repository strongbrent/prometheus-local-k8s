#!/bin/bash

# Update the package index (optional, but recommended)
sudo dnf update -y

# Install the python3-kubernetes package
sudo dnf install -y python3-kubernetes

# Install the kubernetes.core Ansible collection for the current user
ansible-galaxy collection install kubernetes.core
ansible-galaxy collection install community.kubernetes
