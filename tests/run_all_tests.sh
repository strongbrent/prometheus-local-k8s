#!/usr/bin/env bash

files=(
    create_k3d_cluster
    deploy_prometheus_operator
)

for file in "${files[@]}"
do
    echo "Executing test file: ${file}.bats"
    bats ./${file}.bats
done

