#!/bin/bash

# Define the namespace
NAMESPACE=monitoring

# Define service names
PROMETHEUS_SERVICE=prometheus-operator-kube-p-prometheus
ALERTMANAGER_SERVICE=prometheus-operator-kube-p-alertmanager
GRAFANA_SERVICE=prometheus-operator-grafana

# Function to check if a pod is ready
check_pod_ready() {
    local pod_name=$1
    local namespace=$2
    kubectl get pods -n "$namespace" -l "app=$pod_name" -o jsonpath='{.items[*].status.conditions[?(@.type=="Ready")].status}' | grep -q "True"
}

# Function to check if a service has ready endpoints
check_service_ready() {
    local service_name=$1
    local namespace=$2
    kubectl get endpoints "$service_name" -n "$namespace" -o jsonpath='{.subsets[*].addresses[*].ip}' | grep -q .
}

# Function to wait for a resource to be ready
wait_for_ready() {
    local resource_type=$1
    local resource_name=$2
    local namespace=$3
    local retries=30
    local delay=10
    local ready=false

    echo "Waiting for $resource_type $resource_name to become available..."
    for ((i=0; i<retries; i++)); do
        if [ "$resource_type" == "pod" ]; then
            if check_pod_ready "$resource_name" "$namespace"; then
                ready=true
                break
            fi
        elif [ "$resource_type" == "service" ]; then
            if check_service_ready "$resource_name" "$namespace"; then
                ready=true
                break
            fi
        else
            echo "Unknown resource type: $resource_type"
            exit 1
        fi
        sleep "$delay"
    done

    if [ "$ready" == true ]; then
        echo "$resource_type $resource_name is now available."
    else
        echo "$resource_type $resource_name did not become available after $(($retries * $delay)) seconds."
        exit 1
    fi
}

# Start port-forward for Prometheus in a new screen session after it's ready
wait_for_ready service "$PROMETHEUS_SERVICE" "$NAMESPACE"
echo "Starting screen session to forward port for Prometheus..."
screen -dmS prometheus_port_forward bash -c "kubectl port-forward svc/$PROMETHEUS_SERVICE -n $NAMESPACE 9090:9090; exec bash"

# Start port-forward for Alertmanager in a new screen session after it's ready
wait_for_ready service "$ALERTMANAGER_SERVICE" "$NAMESPACE"
echo "Starting screen session to forward port for Alertmanager..."
screen -dmS alertmanager_port_forward bash -c "kubectl port-forward svc/$ALERTMANAGER_SERVICE -n $NAMESPACE 9093:9093; exec bash"

# Start port-forward for Grafana in a new screen session after it's ready
wait_for_ready service "$GRAFANA_SERVICE" "$NAMESPACE"
echo "Starting screen session to forward port for Grafana..."
screen -dmS grafana_port_forward bash -c "kubectl port-forward svc/$GRAFANA_SERVICE -n $NAMESPACE 8080:80; exec bash"

echo "Port-forwarding started in separate screen sessions."