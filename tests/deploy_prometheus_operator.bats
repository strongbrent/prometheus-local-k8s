#!/usr/bin/env bats
# File: tests/deploy_prometheus_operator.bats

@test "Check if Kubernetes namespace 'monitoring' has been created" {
  run kubectl get namespace monitoring
  [ "$status" -eq 0 ]
  [[ $output =~ "monitoring" ]]
}

@test "Check to see if the correct Helm repo has been installed" {
  run helm repo list
  [ "$status" -eq 0 ]
  [[ $output =~ "https://prometheus-community.github.io/helm-charts" ]]
}

@test "Check to ensure that the Prometheus operator custom values file has been removed" {
  run test ! -f /tmp/custom_values.yaml
  [ "$status" -eq 0 ]
}

@test "Ensure that the Prometheus service is running" {
  run kubectl get svc prometheus-operator-kube-p-prometheus -n monitoring
  [ "$status" -eq 0 ]
  [[ $output =~ "prometheus-operator-kube-p-prometheus" ]]
}

@test "Ensure that the Alertmanager service is running" {
  run kubectl get svc prometheus-operator-kube-p-alertmanager -n monitoring
  [ "$status" -eq 0 ]
  [[ $output =~ "prometheus-operator-kube-p-alertmanager" ]]
}

@test "Ensure that the Grafana service is running" {
  run kubectl get svc prometheus-operator-grafana -n monitoring
  [ "$status" -eq 0 ]
  [[ $output =~ "prometheus-operator-grafana" ]]
}

@test "Verify Persistent Volumes" {
  run kubectl get pv -n monitoring
  [ "$status" -eq 0 ]
  [[ "$output" =~ "prometheus" ]]
  [[ "$output" =~ "alertmanager" ]]
  [[ "$output" =~ "grafana" ]]
  [[ "$output" =~ "Bound" ]]
}

@test "Verify Persistent Volume Claims" {
  run kubectl get pvc -n monitoring
  [ "$status" -eq 0 ]
  [[ "$output" =~ "prometheus" ]]
  [[ "$output" =~ "alertmanager" ]]
  [[ "$output" =~ "grafana" ]]
  [[ "$output" =~ "Bound" ]]
}

@test "Ensure that the Prometheus port has been forwarded successfully" {
  run curl --fail --silent --output /dev/null http://localhost:9090/
  [ "$status" -eq 0 ]
}

@test "Ensure that the Alertmanager port has been forwarded successfully" {
  run curl --fail --silent --output /dev/null http://localhost:9093/
  [ "$status" -eq 0 ]
}

@test "Ensure that the Grafana port has been forwarded successfully" {
  run curl --fail --silent --output /dev/null http://localhost:8080/
  [ "$status" -eq 0 ]
}